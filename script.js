const countrySelect = document.getElementById('country-select');

async function fetchCountries() {
  try {
    const response = await fetch('https://restcountries.com/v3.1/all');
    const countries = await response.json();

    countries.forEach(country => {
      const option = document.createElement('option');
      option.value = country.cca3;

      const flagIcon = document.createElement('span');
      flagIcon.className = `flag-icon flag-icon-${country.cca2.toLowerCase()}`;

      const countryName = document.createElement('span');
      countryName.textContent = country.name.common;

      option.appendChild(flagIcon);
      option.appendChild(countryName);

      countrySelect.appendChild(option);
    });
  } catch (error) {
    console.error('Lỗi khi lấy danh sách quốc gia:', error);
  }
}

fetchCountries();

countrySelect.addEventListener('change', function() {
  const selectedCountry = countrySelect.value;
  console.log(`Bạn đã chọn: ${selectedCountry}`);
});
